import sys

def calculate(val_inp, from_inp, to_inp):
    try:
        val_temp = float(val_inp)
    except BaseException:
        return "Wrong input"
    arr = ["mm", "m", "dm", "km", "cm"]
    # print(arr)
    from_temp = str(from_inp)
    to_temp = str(to_inp)
    # print(val_temp, from_temp, to_temp)
    if not from_temp in arr or not to_temp in arr:
        return "Wrong input"
    return val_temp * ((10.0 if "cm" == from_temp else 1.0) *
        (100.0 if "dm" == from_temp else 1.0) *
        (1000.0 if "m" == from_temp else 1.0) *
        (1000000.0 if "km" == from_temp else 1.0)) / (
        (10.0 if "cm" == to_temp else 1.0) *
        (100.0 if "dm" == to_temp else 1.0) *
        (1000.0 if "m" == to_temp else 1.0) *
        (1000000.0 if "km" == to_temp else 1.0))

if(len(sys.argv) == 4):
    val_sys = sys.argv[1]
    from_sys = sys.argv[2]
    to_sys = sys.argv[3]
    print(calculate(val_sys, from_sys, to_sys))

print(calculate(100.0, 'mm', 'cm'))