from my_calc import calculate

def test1():
    assert calculate(100.0, 'mm', 'mm') == 100.0

def test2():
    assert calculate(100.0, 'mm', 'cm') == 10.0

def test3():
    assert calculate(100.0, 'mm', 'dm') == 1.0

def test4():
    assert calculate(100.0, 'mm', 'm') == 0.1

def test5():
    assert calculate(100.0, 'mm', 'km') == 0.0001

def test6():
    assert calculate(100.0, 'cm', 'mm') == 1000.0

def test7():
    assert calculate(100.0, 'cm', 'cm') == 100.0

def test8():
    assert calculate(100.0, 'cm', 'dm') == 10.0

def test9():
    assert calculate(100.0, 'cm', 'm') == 1.0

def test10():
    assert calculate(100.0, 'cm', 'km') == 0.001

def test11():
    assert calculate(100.0, 'dm', 'mm') == 10000.0

def test12():
    assert calculate(100.0, 'dm', 'cm') == 1000.0

def test13():
    assert calculate(100.0, 'dm', 'dm') == 100.0

def test14():
    assert calculate(100.0, 'dm', 'm') == 10.0

def test15():
    assert calculate(100.0, 'dm', 'km') == 0.01

def test16():
    assert calculate(100.0, 'm', 'mm') == 100000.0

def test17():
    assert calculate(100.0, 'm', 'cm') == 10000.0

def test18():
    assert calculate(100.0, 'm', 'dm') == 1000.0

def test19():
    assert calculate(100.0, 'm', 'm') == 100.0

def test20():
    assert calculate(100.0, 'm', 'km') == 0.1

def test21():
    assert calculate(100.0, 'km', 'mm') == 100000000.0

def test22():
    assert calculate(100.0, 'km', 'cm') == 10000000.0

def test23():
    assert calculate(100.0, 'km', 'dm') == 1000000.0

def test24():
    assert calculate(100.0, 'km', 'm') == 100000.0

def test25():
    assert calculate(100.0, 'km', 'km') == 100.0\

def test26():
    assert calculate('100.0a', 'km', 'km') == 'Wrong input'

def test27():
    assert calculate(100.0, 'kmfje', 'km') == 'Wrong input'

def test28():
    assert calculate(100.0, 'km', 5) == 'Wrong input'
